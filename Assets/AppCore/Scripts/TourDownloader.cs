﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TourDownloader : MonoBehaviour
{

	/// <summary>
	/// Occurs every tick of the index download.
	/// Float: percentage progress.
	/// </summary>
	public event Action<float> IndexDownloadTickEvent;

	/// <summary>
	/// Occurs every tick of a tour download.
	/// Float: percentage progress.
	/// Int: tour suite index (which iteration).
	/// </summary>
	public event Action<float> TourDownloadTickEvent;

	/// <summary>
	/// Occurs when the tour suite begins downloading.
	/// Int: the number of tours to be loaded.
	/// </summary>
	public event Action<int> TourLoadingBeginEvent;

	/// <summary>
	/// Occurs when the tour suite loading proccess has concluded.
	/// </summary>
	public event Action TourLoadingEndEvent;

	/// <summary>
	/// Occurs when a tour is determined to be on disk.
	/// (Commonly used to track when a tour is downloaded).
	/// Int: tour suite index.
	/// </summary>
	public event Action<int> TourLoadedEvent;

	private float downloadProgress = 0;

	[SerializeField]
	private TourSaveLoadUtility tourSaveLoadUtil;

	//TODO: Do a Try/Catch for network failures and ask the user if they want to 
	// retry or work offline.
	IEnumerator DownloadTours()
	{
		////////////////
		/// INDEX
		////////////////
		const string baseUrl = "http://touralovr.com/Tours";

		const string indexFilename = "index.txt";
		string indexUrl = baseUrl + "/" + TourSaveLoadUtility.SelectedCategory + "/" + indexFilename;

		Debug.Log("Downloading tour index from:\n" + indexUrl);
		WWW indexDownload = new WWW(indexUrl);

		while( false == indexDownload.isDone )
		{
			if( IndexDownloadTickEvent != null)
			{
				IndexDownloadTickEvent(indexDownload.progress * 100.0f);
			}

			yield return null;
		}

		if (!string.IsNullOrEmpty(indexDownload.error))
		{
			// We are offline...
			if( indexDownload.error.Contains("couldn't connect to host") || 
			   	indexDownload.error.Contains("Couldn't resolve host") )
			{
				// TODO:
				// Instead of aborting, let's inform the user and ask them if 
				// they want to retry, or work offline.

				// HACK:
				// For now, just force offline.
				Debug.LogWarning("No network connection. Going Offline.");
				if( TourLoadedEvent != null )
					TourLoadedEvent(tourSaveLoadUtil.GetTourPathListForCategory(TourSaveLoadUtility.SelectedCategory).Count-1);

			}
			else
			{
				// Some other unknown error.
				Debug.LogError(indexDownload.error);
			}

			yield break;
		}

		string[] tourUrls = GenerateTourUrls(indexDownload.text);

		////////////////
		/// TOURS
		////////////////
		if( TourLoadingBeginEvent != null )
			TourLoadingBeginEvent(tourUrls.Length);

		for (int i = 0; i < tourUrls.Length; i++) 
		{
			string url = tourUrls[i];
			Debug.Log("Downloading tour binary from:\n" + url);
			string tourFilename = System.IO.Path.GetFileName(url);

			WWW tourDownload = new WWW(url);

			do
			{
				downloadProgress = tourDownload.progress * 100.0f;

				if( TourDownloadTickEvent != null )
					TourDownloadTickEvent(downloadProgress);

				yield return new WaitForSeconds(0.3f);
			}
			while( false == tourDownload.isDone );

			if (!string.IsNullOrEmpty(tourDownload.error))
			{
				Debug.LogError(tourDownload.error);
				yield break;
			}

			tourSaveLoadUtil.WriteOut(
				tourDownload.bytes,
				TourSaveLoadUtility.GenerateTourFilepathFromFilename(tourFilename,TourSaveLoadUtility.SelectedCategory));

			if( TourLoadedEvent != null )
				TourLoadedEvent(i);
		}

		// We have downloaded new tours. Let's rescan.
		tourSaveLoadUtil.ScanDiskForTours();

		if( TourLoadingEndEvent != null )
			TourLoadingEndEvent();
	}

	public void LoadToursFromCategory( string category )
	{
		TourSaveLoadUtility.CreateCategory(category); // Only creates if non-existent.
		TourSaveLoadUtility.SetSelectedCategory(category);
		StartCoroutine(DownloadTours());
	}

	private string[] GenerateTourUrls( string indexFileContents)
	{
		/*
		* FORMAT: [Binary Version] [TimeStamp Uploaded] [URL]
		* 	We check version in order to confirm that the file has not been updated with an older builder.
		*	We check Timestamp to ensure that we have the same version of the tour binary, otherwise, update.
		*	We check the URL to download the binary if necessary.
		**/ 
		string[] indexBreakdown = indexFileContents.Split( '\n' );
		List<string> urlList = new List<string>(indexBreakdown.Length / 3);

		for (int i = 0; i < indexBreakdown.Length; i += 3) 
		{
			string binaryVersion = indexBreakdown[i + 0];
			string timeStamp = indexBreakdown[i + 1];
			string binaryUrl = indexBreakdown[i + 2];

			string tourFilename = System.IO.Path.GetFileName(binaryUrl);
			if( TourSaveLoadUtility.DoesTourBinaryExist( tourFilename, TourSaveLoadUtility.SelectedCategory) )
			{
				// To verify the contents of the existing binary file, we must select it.
				string fullPath = TourSaveLoadUtility.GenerateTourFilepathFromFilename(tourFilename, TourSaveLoadUtility.SelectedCategory);

				TourSaveLoadUtility.SetSelectedFilename( fullPath );

				string fileDateString = tourSaveLoadUtil.ParseTourTimestamp();
				DateTime fileDate = DateTime.Parse( fileDateString );
				DateTime indexDate = DateTime.Parse( timeStamp );
				// The file exists, but is it the latest?
				if( DateTime.Compare(fileDate,indexDate) >= 0 )
				{
					// We have the same time stamp. Could it somehow be a fluke?
					// The final test: Version.
					float fileVersion = float.Parse(tourSaveLoadUtil.ParseTourVersion());
					if( fileVersion >= float.Parse(binaryVersion) )
					{
						// Okay, this tour checks out. It is valid and up-to-date. No need to re-download.
						continue; // Move onto the next tour in the index.
					}
				}
			}

			urlList.Add( binaryUrl );
		}

		return urlList.ToArray();
	}
}
