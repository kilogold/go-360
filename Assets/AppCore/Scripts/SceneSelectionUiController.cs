using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

[RequireComponent(typeof(TourSaveLoadUtility))]
public class SceneSelectionUiController : MonoBehaviour
{
    [SerializeField]
    GameObject sceneSelectionEntryPrefab;

    [SerializeField]
    RectTransform sceneSelectionEntryPanel;

	[SerializeField]
	CanvasGroup selectionFollowupPanel;

	[SerializeField]
	UiPanelFader panelFader;

    // Use this for initialization
    public void PopulateSceneSelection()
    {
		var tourSaveLoadUtil = GameObject.FindObjectOfType<TourSaveLoadUtility>();
		int totalTours = tourSaveLoadUtil.GetTourPathListForCategory(TourSaveLoadUtility.SelectedCategory).Count;

        for (int i = 0; i < totalTours; i++)
        {
            GameObject newEntry = Instantiate(sceneSelectionEntryPrefab) as GameObject;
            newEntry.transform.SetParent(sceneSelectionEntryPanel, false);
            SceneSelectionEntry selectedEntry = newEntry.GetComponent<SceneSelectionEntry>();

			string tourFilename = tourSaveLoadUtil.GetTourFileName(i, TourSaveLoadUtility.SelectedCategory);

			selectedEntry.Initialize(this, i, tourFilename);
		}
    }

	public void ClearSceneSelection()
	{
		foreach (RectTransform item in sceneSelectionEntryPanel) 
		{
			Destroy(item.gameObject);
		}
	}

	void Update()
	{
		if( Input.GetKeyDown(KeyCode.A))
		{
			TourSaveLoadUtility.SetSelectedCategory("RealityRealty");
			PopulateSceneSelection();
		}
	}

    public void EntryClicked( int selectedEntryIndex )
    {
        Transform selectedEntryTransform = sceneSelectionEntryPanel.GetChild(selectedEntryIndex);
        Debug.Log("Selection (" + selectedEntryIndex + ") for id " + selectedEntryTransform.GetComponent<SceneSelectionEntry>().GetInstanceID());

		TourSaveLoadUtility.SetSelectedFilename(selectedEntryIndex);
		panelFader.FadeToPanel(selectionFollowupPanel);
		ClearSceneSelection();
    }
}
