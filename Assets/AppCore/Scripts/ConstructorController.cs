using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;

namespace TourBuilder
{
	[RequireComponent(typeof(TourNavigator))]
	public class ConstructorController : MonoBehaviour
	{
	    private string photosphereFilepath;

		private LinkData SelectedLink;

	    [SerializeField]
	    private Transform roomSphere;

	    [SerializeField]
	    private float linkToCameraDistance;

	    [SerializeField]
	    private Renderer currentSphereRenderer;

	    [SerializeField]
	    private TourSaveLoadUtility tourSaveLoadUtil;

	    [SerializeField]
	    private Canvas uiCanvas;

	    /// <summary>
	    /// Attempts to load an already existing TOUR from file to continue working on.
	    /// Currently, the images int the Constructor Folder MUST match what is in the binary
	    /// due to how the texture loader works. WIP
	    /// </summary>
	    [SerializeField]
	    private int loadFromBinaryAtIndex;

	    private TourNavigator tourNavigator;
	    private ConstructorTour tourInstance;

		/// <summary>
		/// The tour's category when using the builder.
		/// Every tour has a category.
		/// </summary>
		public readonly string TourSaveLoadCategory = "Builder";

	    void Awake()
	    {
	        tourInstance = new ConstructorTour();

	        tourNavigator = GetComponent<TourNavigator>();
	        tourNavigator.Initialize(tourInstance);
	    }

		public void NavigateToSelectedRoom()
		{
			tourNavigator.NavigateToRoom(SelectedLink.GetComponent<LinkData>()); 
		}

	    void Update()
	    {
			if( false == ObtainLink() )
			{
				DeselectLink();
			}
	    }

		public void SaveTour( string filename )
		{
			string filepath = TourSaveLoadUtility.GenerateTourFilepathFromFilename(filename, "Builder");
			tourSaveLoadUtil.WriteOut( tourInstance, filepath );
		}

	    bool ObtainLink()
	    {
	        RaycastHit hitInfo;

	        bool didHit = Physics.Raycast(
	            Camera.main.transform.position,
	            Camera.main.transform.forward,
	            out hitInfo);

	        if( didHit )
	        {
	            if (SelectedLink != null)
	            {
					//Only acknowledge the hit if it's different from the one we already have.
					//This allows us to constantly attempt at obtaining a link every frame without
					//having to keep track of the previously obtained link externally.
					if( hitInfo.transform.gameObject.GetInstanceID() == SelectedLink.gameObject.GetInstanceID() )
					{
						return true;
					}
					else
					{
	                	DeselectLink();
					}
	            }

	            SelectLink( hitInfo.transform.gameObject );
	        }

	        return didHit;
	    }

	    void SelectLink( GameObject linkRef )
	    {
	        SelectedLink = linkRef.GetComponent<LinkData>();
	        SelectedLink.GetComponent<Renderer>().material.color = Color.yellow;
	    }

	    void DeselectLink()
	    {
	        if (SelectedLink != null)
	        {
	            SelectedLink.GetComponent<Renderer>().material.color = Color.cyan;
				SelectedLink = null;
	        }
	    }

		/// <summary>
		/// Creates a room link.
		/// </summary>
		/// <param name="roomID">Room ID to the linked room.</param>
	    private void CreateLink( int roomID )
	    {
	        //Position at camera's forward vector offset
	        Vector3 linkPosition = Camera.main.transform.position + Camera.main.transform.forward * linkToCameraDistance;

	        GameObject linkPortalPrefab = Resources.Load("Prefabs/PortalLink") as GameObject;
	        GameObject linkPortalInstance = Instantiate(linkPortalPrefab) as GameObject;

	        linkPortalInstance.transform.parent = roomSphere.transform;
	        linkPortalInstance.transform.position = linkPosition;
	        linkPortalInstance.transform.rotation = Quaternion.LookRotation(linkPortalInstance.transform.position - Camera.main.transform.position);
			linkPortalInstance.GetComponent<LinkData>().LinkedRoomID = roomID;

	        // Although it seems redundant to create "RawLinkData" instead of using "LinkData", we want the raw data
	        // to be easily/readily/independently/portablly serializable, and not part of the scene hierarchy. 
	        // By doing this extra work, we don't have to worry about the instance behaviors or any functionality 
	        // outside of our direct control.
	        TourNode currentRoomNode = tourInstance.GetRoom(tourNavigator.CurrentRoomID);
	        TourNode.RawLinkData currentRoomNodeLinkData = new TourNode.RawLinkData(
	            linkPortalInstance.GetInstanceID(), 
	            roomID, 
	            linkPortalInstance.transform.position );

	        currentRoomNode.RoomLinks.Add( currentRoomNodeLinkData );
	    }

	    /// <summary>
	    /// Edits the current room id value
	    /// </summary>
	    /// <param name="linkRoomID"> Empty string if value is unassigned (-1) </param>
	    private void EditLink( string linkRoomID )
	    {
	        if (false == string.IsNullOrEmpty(linkRoomID))
	        {
	            Debug.Log("Selected Link new value: " + linkRoomID);

	            int intLinkRoomID = int.Parse(linkRoomID);
	            SelectedLink.LinkedRoomID = intLinkRoomID;
	            
	            TourNode selectedLinkNode = tourInstance.GetRoom( tourNavigator.CurrentRoomID );
	            TourNode.RawLinkData selectedLinkNodeData = selectedLinkNode.GetRoomLinkForInstanceID( SelectedLink.GetInstanceID() );
	            selectedLinkNodeData.LinkRoomID = intLinkRoomID;
	        }
	    }

	    public void DeleteLink()
	    {
	        if( SelectedLink != null )
	        {
				TourNode selectedLinkNode = tourInstance.GetRoom(SelectedLink.LinkedRoomID);
	            TourNode.RawLinkData selectedLinkNodeData = selectedLinkNode.GetRoomLinkForInstanceID( SelectedLink.GetInstanceID() );
	            selectedLinkNode.RoomLinks.Remove(selectedLinkNodeData);
				Destroy(SelectedLink.gameObject);
	        }
	    }

		public void SetInitialRoom( LinkPanoSelectionMeta selectionMeta )
		{
			tourInstance.ResetRooms();
			CreateRoom( selectionMeta.RoomTextureLoaderImageIndex );
			tourNavigator.NavigateToRoom( 0 );
		}

		/// <summary>
		/// Creates a new room in the Tour.
		/// </summary>
		/// <returns>The new room's index within the Tour.</returns>
		/// <param name="imageIndex">Image pano index from the RoomTextureLoader.</param>
		private int CreateRoom( int imageIndex )
		{
			TourNode newRoom = new TourNode( imageIndex );
			return tourInstance.AddRoom(newRoom);
		}

		public void LinkNewRoom( LinkPanoSelectionMeta selectionMeta )
		{
			int newRoomIndex = CreateRoom( selectionMeta.RoomTextureLoaderImageIndex );
			CreateLink( newRoomIndex );
		}

		public void LinkExistingRoom( LinkRoomSelectionMeta selectionMeta )
		{
			CreateLink( selectionMeta.TourRoomIndex );
		}

		public int[] GetRoomTextureIndiciesFromTour()
		{
			int totalRoomsInTour = tourInstance.GetRoomCount();
			int[] roomTextureIndices = new int[totalRoomsInTour];
			for (int i = 0; i < totalRoomsInTour; i++) 
			{
				roomTextureIndices[i] = tourInstance.GetRoom(i).RoomTextureIndex;
			}

			return roomTextureIndices;
		}

		public bool IsTourInitialized()
		{
			if( tourInstance != null )
			{
			  return tourInstance.GetRoomCount() > 0;
			}

			return false;
		}
	}
}
