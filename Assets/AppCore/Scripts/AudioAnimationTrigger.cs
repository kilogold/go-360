﻿using UnityEngine;
using System.Collections;

public class AudioAnimationTrigger : MonoBehaviour
{
	public AudioSource audioSource;

	public void PlayAudio()
	{
		audioSource.Play();
	}
}
