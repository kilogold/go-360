using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Text.RegularExpressions;

/// <summary>
/// a loader with method to return texture for Room ID.
/// Depending on the mode, the loader will perform a different lookup.
/// 
/// User Mode:
///     Obtain textures from binary File I/O
///     
/// Constructor Mode:
///     Obtain textures from directory
///     
/// </summary>
public class RoomTextureLoaderFactory
{
    static public RoomTextureLoader CreateRoomTextureLoader()
    {
		if( Application.loadedLevelName == "TourBuilder")
			return new ConstructorRoomTextureLoader();
		else
		{
			return new PlayerRoomTextureLoader();
		}
    }
}

public abstract class RoomTextureLoader
{
    protected TourSaveLoadUtility utilSaveLoad;

    protected RoomTextureLoader()
    {
        utilSaveLoad = GameObject.FindObjectOfType<TourSaveLoadUtility>();
    }

    public abstract Texture2D GetRoomTexture(int roomId);
    public abstract Texture2D GetRoomTexture(TourNode roomNodeData);
    public abstract Texture2D[] GetTotalRoomTextures();
    public abstract int RoomTexturesCount();
	public abstract Texture2D GetInitialRoomThumbnailTexture();


    protected Texture2D LoadTextureFromBytes(ref byte[] bytes)
    {
        Texture2D roomTexture = new Texture2D(2, 2);
        roomTexture.LoadImage(bytes);

        return roomTexture;
    }
}

public class PlayerRoomTextureLoader : RoomTextureLoader
{
    public override Texture2D GetRoomTexture(int roomID)
    {
        //TODO:
        // How to load? We need to load from TourNode's byte offset.
        // reader.BaseStream.Seek( [RoomTextureByteLengthBinaryOffset] , SeekOrigin.Current);
        //HACK:
        // For now, let's just seek the whole thing every time.

        byte[] textureBytes = utilSaveLoad.ParseSingleTourNodeRoomTextureBytesFromBinaryAtIndex(roomID);

        return LoadTextureFromBytes(ref textureBytes);
    }

    public override Texture2D[] GetTotalRoomTextures()
    {
        int totalRooms = utilSaveLoad.ParseTotalRoomsCount();
        Texture2D[] returnVal = new Texture2D[totalRooms];

        for (int curIndex = 0; curIndex < totalRooms; curIndex++)
        {
            byte[] textureBytes = utilSaveLoad.ParseSingleTourNodeRoomTextureBytesFromBinaryAtIndex(curIndex);
            returnVal[curIndex] = LoadTextureFromBytes(ref textureBytes);
        }

        return returnVal;
    }

    public override Texture2D GetRoomTexture(TourNode roomNodeData)
    {
        byte[] textureBytes = 
            utilSaveLoad.ParseTourNodeTextureBytesFromBinaryOffset( roomNodeData.RoomTextureByteLengthBinaryOffset );

        return LoadTextureFromBytes( ref textureBytes);
    }

    public override int RoomTexturesCount()
    {
        return utilSaveLoad.ParseTotalRoomsCount();
    }

	public override Texture2D GetInitialRoomThumbnailTexture()
	{
		byte[] textureBytes = 
			utilSaveLoad.ParseTourThumbnailTextureBytes();

		return LoadTextureFromBytes(ref textureBytes);
	}
}


// TODO: 
// Make this class not handle urls for AvailableFilenames, and receive them as parameters.
// Make tour save/load utility handle the url and contain an instance of this.
public class ConstructorRoomTextureLoader : RoomTextureLoader
{
    private Texture2D GetRoomTexture(string filenamePath)
    {
        byte[] bytes = File.ReadAllBytes(filenamePath);

        return LoadTextureFromBytes(ref bytes);
    }

    public override Texture2D GetRoomTexture(int roomTextureID)
    {
        List<FileInfo> roomTextureList = GetRoomTextureDirectoryListing();

        byte[] bytes = File.ReadAllBytes(roomTextureList[roomTextureID].FullName);

        return LoadTextureFromBytes(ref bytes);
    }

    public override Texture2D GetRoomTexture(TourNode roomNodeData)
    {
		return GetRoomTexture(roomNodeData.RoomTextureIndex);
    }

    /// <summary>
    /// Gets an ordered list of all room textures.
    /// </summary>
    /// <returns>Array of room textures</returns>
    public override Texture2D[] GetTotalRoomTextures()
    {
        List<FileInfo> textureFileList = GetRoomTextureDirectoryListing();
        Texture2D[] returnVal = new Texture2D[textureFileList.Count];

        for (int curIndex = 0; curIndex < textureFileList.Count; curIndex++)
        {
            returnVal[curIndex] = GetRoomTexture(textureFileList[curIndex].FullName);
        }

        return returnVal;
    }

    public override int RoomTexturesCount()
    {
       return GetRoomTextureDirectoryListing().Count;
    }


    private List<FileInfo> GetRoomTextureDirectoryListing()
    {
        string[] roomTexturePath = 
        {
            "/mnt/extSdCard/DCIM/Camera",
            "/mnt/sdcard/DCIM/Camera",
            "C:/Users/Kelvin/Desktop/T",
			"/Users/kelvinbonilla/Desktop/Development/T"
        };

        List<FileInfo> filterList = new List<FileInfo>();

		bool noDirectoryFound = true;
        foreach (string path in roomTexturePath)
        {
            // Only process the paths that exist.
            if (false == Directory.Exists(path))
            {
                continue;
            }

			noDirectoryFound = true;
			Debug.Log("Directory found: " + path);
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            filterList.AddRange(dirInfo.GetFiles());
        }
		if(noDirectoryFound)
		{
			Debug.Log("No directory found.");
		}

        // Remove all non-jpg files to avoid processing unexpected files.
        filterList.RemoveAll(elem => false == string.Equals(".jpg", Path.GetExtension(elem.Name), StringComparison.OrdinalIgnoreCase));

        // Remove all images that don't conform to Google Camera's Photosphere filenaming convention.
        filterList.RemoveAll(elem => false == Regex.IsMatch( elem.Name, "^PANO_.*.jpg"));        

        Debug.Log("Files: " + filterList.Count);

        string list = string.Empty;

        foreach (FileInfo item in filterList)
        {
            list += (item.Name + "\n");
        }
        Debug.Log(list);

        return filterList;
    }

	public override Texture2D GetInitialRoomThumbnailTexture()
	{
		throw new NotImplementedException();
	}
}

