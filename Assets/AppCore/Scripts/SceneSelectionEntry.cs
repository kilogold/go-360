using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using TouchScript.Gestures;

[RequireComponent(typeof(GazeInteractionReceiver))]
public class SceneSelectionEntry : MonoBehaviour
{
    private Material entrySphereMaterial;
    private SceneSelectionUiController sceneSelectionUiController;
    private int filenameIndex;
	private TapGesture tapGesture;

	void Awake()
	{
		tapGesture = GetComponent<TapGesture>();
		tapGesture.Tapped += OnTap;
	}

	void OnDestroy()
	{
		tapGesture.Tapped -= OnTap;
	}

    public void Initialize(SceneSelectionUiController sceneSelectionUiControllerIn, int filenameIndexIn, string entryName )
    {
        filenameIndex = filenameIndexIn;
        sceneSelectionUiController = sceneSelectionUiControllerIn;
        RoomTextureLoader tl = RoomTextureLoaderFactory.CreateRoomTextureLoader();

		// We need to set the selected filename because the texture loader assumes the currently
		// selected filename for texture loading.
		// TODO: Turn "GetRoomTexture( int roomIndex )" into something like,
		//       "GetRoomTexture( int roomIndex, string category )".
		TourSaveLoadUtility.SetSelectedFilename(filenameIndex);

		Texture2D tex= tl.GetInitialRoomThumbnailTexture();

        entrySphereMaterial = transform.GetChild(0).GetComponent<Renderer>().material;
        entrySphereMaterial.mainTexture = tex;

		GetComponentInChildren<Text>().text = entryName;
    }

    void Update()
    {
        float rotationSpeed = Time.deltaTime/20.0f;
        entrySphereMaterial.mainTextureOffset += new Vector2(rotationSpeed, 0);
    }

    void OnGaze()
    {
		//ExecuteButton();
    }

	void OnTap( object sender, System.EventArgs e )
	{
		ExecuteButton();
	}

	private void ExecuteButton()
	{
		sceneSelectionUiController.EntryClicked(filenameIndex);
	}
}
