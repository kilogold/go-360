﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Metadata when selecting an existing room from a scrolling
/// list, such as the one from Tour Builder when adding an existing room.
/// </summary>
public class LinkRoomSelectionMeta : MonoBehaviour
{
	public int TourRoomIndex;
}
