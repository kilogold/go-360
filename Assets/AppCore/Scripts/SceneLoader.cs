﻿using UnityEngine;
using System.Collections;

public class SceneLoader : MonoBehaviour
{
	public void LoadLevel( int Index )
	{
		Application.LoadLevel(Index);
	}

	public void LoadLevel( string LevelName )
	{
		Application.LoadLevel( LevelName );
	}
}
