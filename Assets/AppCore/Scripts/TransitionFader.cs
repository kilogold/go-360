﻿using UnityEngine;
using System.Collections;

public class TransitionFader : MonoBehaviour
{
	public float transitionTime;

	// Use this for initialization
	IEnumerator Start ()
	{
		yield return null;

		var sharedMat = GetComponent<Renderer>().material;

		float totalTime = transitionTime;

		yield return null;

		while( transitionTime > 0)
		{
			transitionTime = Mathf.Clamp(transitionTime - Time.deltaTime, 0,totalTime);

			float newAlpha = Mathf.Lerp(0,1,transitionTime/totalTime);
			sharedMat.color = new Color(
				sharedMat.color.r ,
				sharedMat.color.g ,
				sharedMat.color.b ,
				newAlpha );

			yield return null;
		}

		Destroy (gameObject);
	}
}
