﻿using UnityEngine;
using System;
using System.Collections;

public class UiPanelFader : MonoBehaviour
{
	public event Action<CanvasGroup> FadeComplete;

	[Range(0.1f,2.0f)]
	public float fadeTimeInSeconds = 1;
	public CanvasGroup[] uiPanels;

	private bool isFading = false;
	private CanvasGroup currentPanel;

	void Start()
	{
		foreach (var panel in uiPanels) 
		{
			if( panel.gameObject.activeInHierarchy )
			{
				currentPanel = panel;
				break;
			}
		}
	}

	public void FadeToPanel( CanvasGroup panel )
	{
		if( isFading )
			return;

		foreach (var item in uiPanels) 
		{
			if( panel == item )
			{
				StartCoroutine(FadeCoroutine(panel));
				return;
			}
		}

		Debug.LogError("Specified panel is not part of the list.");
	}

	private IEnumerator FadeCoroutine( CanvasGroup fadeInPanel )
	{
		isFading = true;

		foreach (var panel in uiPanels) 
		{
			// Make others transparent so we don't see initial transition artefacts.
			if( panel != currentPanel )
			{
				panel.alpha = 0;
			}
			else
			{
				// We want to avoidthe user from spamming the UI.
				currentPanel.interactable = false;
				currentPanel.blocksRaycasts = false;
			}
		}


		float overallProgress = 0;

		while( overallProgress < 1.0f )
		{
			overallProgress += Time.deltaTime / fadeTimeInSeconds;
			fadeInPanel.alpha = Mathf.Lerp(0,1,overallProgress);
			currentPanel.alpha = Mathf.Lerp(1,0,overallProgress);

			yield return null;
		}

		foreach (var panel in uiPanels) 
		{
			// Let's disable any invisible panels
			// (Floats are a little imprecise. Let's not check for actual Zero)
			if( panel.alpha < 0.1f )
			{
				panel.interactable = false;
			}
		}

		currentPanel = fadeInPanel;
		currentPanel.interactable = true;
		currentPanel.blocksRaycasts = true;

		isFading = false;

		if( FadeComplete != null )
			FadeComplete( fadeInPanel );
	}
}
