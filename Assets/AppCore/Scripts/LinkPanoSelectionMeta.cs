﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Metadata when selecting a pano image from a scrolling
/// list, such as the one from Tour Builder when adding a 
/// new room.
/// </summary>
public class LinkPanoSelectionMeta : MonoBehaviour
{
	public int RoomTextureLoaderImageIndex;
}
