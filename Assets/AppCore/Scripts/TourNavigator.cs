using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// This class keeps track of where the user is in the tour.
/// It can request to transition to the next room (while handling it's own transitions).
/// The TourNavigator must be initialized before it can be used (where we pass in the actual Tour instance)
/// When in user-mode, the Tour instance is loaded via File I/O.
/// When in build-mode, the Tour instance is passed in as a reference from the TourConstructor.
/// </summary>
public class TourNavigator : MonoBehaviour
{
    private Tour tourInstance;
    private GameObject roomSphere;
    private RoomTextureLoader roomTextureLoader;

    public int CurrentRoomID { private set; get; }

    public void Initialize(Tour tourInstanceIn)
    {
        roomTextureLoader = RoomTextureLoaderFactory.CreateRoomTextureLoader();
        tourInstance = tourInstanceIn;
        roomSphere = GameObject.FindGameObjectWithTag("RoomSphere");
    }

    public void NavigateToRoom( int roomID )
    {
        Debug.Log("Now loading RoomID: " + roomID);

        CurrentRoomID = roomID;

        ResetRoomSphere();
    }

	public void NavigateToRoom( LinkData roomLink )
	{
		NavigateToRoom( roomLink.LinkedRoomID );
	}

    public void ResetRoomSphere()
    {
        foreach (Transform link in roomSphere.transform)
        {
            Destroy(link.gameObject);
        }

		if(0 == tourInstance.GetRoomCount() )
		{
			//Chances are we only call this in the beginning.
			Debug.Log("There are no rooms in this tour.");
			return;
		}

        TourNode roomNodeData = tourInstance.GetRoom(CurrentRoomID);

        // If we have room node data, let's load according to that.
        if (null != roomNodeData)
        {
            Destroy(roomSphere.GetComponent<Renderer>().material.mainTexture);

			roomSphere.GetComponent<Renderer>().material.mainTexture = roomTextureLoader.GetRoomTexture(roomNodeData);
            StartCoroutine(lol());

            foreach (TourNode.RawLinkData curRawLink in roomNodeData.RoomLinks)
            {
                GameObject linkPortalPrefab = Resources.Load("Prefabs/PortalLink") as GameObject;
                GameObject linkPortalInstance = Instantiate(linkPortalPrefab);

                linkPortalInstance.transform.parent = roomSphere.transform;
                linkPortalInstance.transform.position = curRawLink.LinkPosition;
                linkPortalInstance.transform.rotation = Quaternion.LookRotation(linkPortalInstance.transform.position - Camera.main.transform.position);
                linkPortalInstance.GetComponent<LinkData>().LinkedRoomID = curRawLink.LinkRoomID;
                curRawLink.LinkDataInstanceID = linkPortalInstance.GetInstanceID();
            }
        }
        else
        {
            Debug.LogError("Missing node data for Room ID: " + CurrentRoomID);
        }
    }

    IEnumerator lol()
    {
        yield return Resources.UnloadUnusedAssets();
    }
}
