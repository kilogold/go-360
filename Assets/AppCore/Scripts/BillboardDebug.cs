using UnityEngine;
using System.Collections;

public class BillboardDebug : MonoBehaviour
{

    void LateUpdate()
    {
        Debug.DrawRay(transform.position, transform.forward, Color.green);
    }

}
