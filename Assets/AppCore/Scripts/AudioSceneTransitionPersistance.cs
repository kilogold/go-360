﻿using UnityEngine;
using System.Collections;

public class AudioSceneTransitionPersistance : MonoBehaviour
{
	AudioSource audioSource;
	int initialLevel = -1;

	// Use this for initialization
	void Start ()
	{
		initialLevel = Application.loadedLevel;
		DontDestroyOnLoad(gameObject);
		audioSource = GetComponent<AudioSource>();
	}
	
	IEnumerator OnLevelWasLoaded( int level )
	{
		Debug.Log("New Level Loaded");
		// If the level we are loading is different
		// OR
		// If upon loading a new level, we had previously initialized...
		if( initialLevel != Application.loadedLevel ||
			initialLevel != -1 )
		{
			// Let the sound play until it is over, then destroy.
			while( audioSource.isPlaying )
			{
				yield return null;
			}

			Debug.Log("Destroying");
			Destroy (gameObject);
		}
	}
}
