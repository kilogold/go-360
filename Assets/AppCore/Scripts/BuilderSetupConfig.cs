using UnityEngine;
using System.Collections;

/// <summary>
/// Intended as purely a container class.
/// This class shall make no operations.
/// </summary>
public class BuilderSetupConfig : MonoBehaviour
{
    public int initialRoomId = -1;

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

}
