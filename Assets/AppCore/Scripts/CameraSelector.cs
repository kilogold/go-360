using UnityEngine;
using System.Collections;

public enum CAMERA_CONFIG 
{ 
	StandardProjection,
	Cardboard,
	GearVR
}

/// <summary>
/// Selects the Camera Mode to view the tours with.
/// This is only relevant to aspects of the TourPlayer.
/// </summary>
public class CameraSelector : MonoBehaviour
{
	/// <summary>
	/// The player prefs key-value for the camera mode.
	/// </summary>
	public const string PlayerPrefs_Mode = "CamMode";

    [SerializeField]
    GameObject StandardProjection;
    
    [SerializeField]
    GameObject GoogleCardboard;

	[SerializeField]
	GameObject GearVR;

    private GameObject[] cameraConfigs;
	
	/// <summary>
	/// The scenes where the camera selector should operate.
	/// </summary>
	private string[] Scenes = new string[] 
	{
		"PropertySelection", 
		"TourPlayer"
	};

    static CameraSelector singleInstance = null;
    void Awake()
    {
        if (singleInstance == null)
        {
            singleInstance = this;
            DontDestroyOnLoad(gameObject);

            cameraConfigs = new GameObject[] {
            StandardProjection,
            GoogleCardboard,
			GearVR
            };
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        foreach (GameObject curCamConfig in cameraConfigs)
        {
            if( curCamConfig != null )
                curCamConfig.SetActive(false);
        }

		int cameraConfig = PlayerPrefs.GetInt(PlayerPrefs_Mode);
        cameraConfigs[cameraConfig].SetActive(true);
    }

	public void Purge()
	{
		singleInstance = null;
		Destroy(gameObject);
	}

	public void OnLevelWasLoaded( int levelIndex)
	{
		foreach (var level in Scenes) 
		{
			if( Application.loadedLevelName == level )
			{
				return;
			}
		}

		Purge();
	}
}
