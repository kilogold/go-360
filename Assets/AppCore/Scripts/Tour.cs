using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface Tour
{
    TourNode GetRoom(int roomID);
    int GetRoomCount();

}

public class PlayerTour : Tour
{
    private TourSaveLoadUtility tourSaveLoadUtil;

    public PlayerTour( TourSaveLoadUtility tourSaveLoadUtilIn )
    {
        tourSaveLoadUtil = tourSaveLoadUtilIn;
    }

    public TourNode GetRoom( int roomID)
    {
        return tourSaveLoadUtil.ParseSingleTourNodeFromBinaryAtIndex(roomID);
    }

    public int GetRoomCount()
    {
		return tourSaveLoadUtil.ParseTotalRoomsCount();
    }
}

namespace TourBuilder
{
	public class ConstructorTour : Tour
	{
	    private List<TourNode> roomsList;

	    public ConstructorTour( int initialCapacity = 1 )
	    {
			roomsList = new List<TourNode>( initialCapacity );
	    }

	    public ConstructorTour(TourSaveLoadUtility saveLoadUtil  )
	    {
	        int roomCount = saveLoadUtil.ParseTotalRoomsCount();
	        roomsList = new List<TourNode>(roomCount);

	        for (int curIndex = 0; curIndex < roomCount; curIndex++)
	        {
	            AddRoom(saveLoadUtil.ParseSingleTourNodeFromBinaryAtIndex(curIndex));
	        }
	    }

	    public void ResetRooms()
	    {
			roomsList.Clear();
	    }

	    public void SetRoom(int roomID, TourNode newRoomData)
	    {
	        if( roomsList == null )
	        {
	            Debug.LogError("Tour is not initialized.");
	            return;
	        }

	        if( roomID < 0 || roomID >= roomsList.Count )
	        {
	            Debug.LogError("Room ID is out of bounds.");
	            return;
	        }

	        if( roomsList[roomID] != null )
	        {
	            Debug.LogWarning("Overwriting room with an existing room.");
	        }
	    }

		/// <summary>
		/// Adds the room to the tour.
		/// </summary>
		/// <returns>The room index within the tour.</returns>
		/// <param name="newRoomData">An initialized TourNode with the new room data</param>
		public int AddRoom( TourNode newRoomData )
		{
			roomsList.Add(newRoomData);
			Debug.Log("New room has been added.");
			return roomsList.Count-1;
		}

	    public TourNode GetRoom( int roomID)
	    {
	        if( roomID >= 0 && roomID < roomsList.Count )
	        {
	            return roomsList[roomID];
	        }

	        return null;
	    }

	    public int GetRoomCount()
	    {
	        return roomsList.Count;
	    }
	}
}
