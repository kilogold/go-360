using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollRectScroller : MonoBehaviour
{
    [SerializeField]
    private ScrollRect srollRect;

    [SerializeField]
    private float scrollSpeed;

    private void OnGaze()
    {
        srollRect.verticalNormalizedPosition += scrollSpeed * Time.deltaTime;
        srollRect.verticalNormalizedPosition = Mathf.Clamp01(srollRect.verticalNormalizedPosition);
    }
}
