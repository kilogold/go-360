﻿using UnityEngine;
using ProgressBar;
using System.Collections;
using System;

public class ModeSelectUiController : MonoBehaviour 
{
	public ProgressBarBehaviour sceneLoadProgress;

	private AsyncOperation levelLoader;
	
	/// <summary>
	/// Sets the tour player mode based on the enum config.
	/// Unity should allow enums to be serilized for Editor OnClick()
	/// events...
	/// </summary>
	/// <param name="enumConfig">Enum config.</param>	
	public void LoadProperySelectionWithViewerMode( int enumConfig )
	{
		if( false == Enum.IsDefined(typeof(CAMERA_CONFIG),enumConfig) )
		{
			Debug.LogError("Undefined enum value in UI button");
			return;
		}

		// We've made a selection, let's avoid the user spamming the UI.
		GetComponent<CanvasGroup>().interactable = false;

		// Let's remember the mode, so we can load it when on the next scene.
		PlayerPrefs.SetInt(CameraSelector.PlayerPrefs_Mode, enumConfig);

		StartCoroutine(LoadPropertySelectionScene());
	}

	private IEnumerator LoadPropertySelectionScene()
	{
		sceneLoadProgress.gameObject.SetActive(true);

		levelLoader = Application.LoadLevelAsync("TourPlayer");
		levelLoader.allowSceneActivation = false;

		while(levelLoader.progress < 0.9f)
		{
			sceneLoadProgress.Value = levelLoader.progress * 100.0f;
			yield return null;
		}

		// We are done loading, let's clamp.
		sceneLoadProgress.Value = 100.0f;

		//Once the progress is complete, we will wait until the progress bar is ready to present the scene.
	}

	public void PresentNextScene()
	{
		levelLoader.allowSceneActivation = true;
	}
}
