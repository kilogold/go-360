using UnityEngine;
using System.Collections;
using System;

public class GazeInteraction : MonoBehaviour
{
    private float currentInteractionTime = 0;
    private GazeInteractionReceiver gazeReceiver;

    private Vector3 SelectButtonDownPosition;

    // Update is called once per frame
    void Update()
    {
        GetGazedReceiver();

        if (gazeReceiver != null)
        {
            switch (gazeReceiver.interactionMode)
            {
                case InteractionMode.Input:
                    {
                        if (Input.GetButtonDown("Select"))
                        {
                            SelectButtonDownPosition = Input.mousePosition;
                        }

                        if (Input.GetButtonUp("Select"))
                        {
                            if ((Input.mousePosition - SelectButtonDownPosition).magnitude < 0.5f)
                            {
                                    gazeReceiver.ExecuteOnGaze();
                            }
                        }
                    }
                    break;
                case InteractionMode.Timer:
                    {
                        currentInteractionTime -= Time.deltaTime;

                        if (currentInteractionTime <= 0)
                        {
                            gazeReceiver.ExecuteOnGaze();
                            currentInteractionTime = gazeReceiver.interactionTimeThreshold;
                        }
                    }
                    break;
            }
        }
    }

    private void GetGazedReceiver()
    {
        RaycastHit hitInfo;

        bool didHit = Physics.Raycast(
            transform.position,
            transform.forward,
            out hitInfo);

        if( didHit )
        {
            // Will return null if not found.
            GazeInteractionReceiver target = hitInfo.collider.GetComponent<GazeInteractionReceiver>();

            if( target != gazeReceiver)
            {
                gazeReceiver = target;

                if (target != null)
                {
                    currentInteractionTime = target.interactionTimeThreshold;
                }
            }
        }
        else
        {
            gazeReceiver = null;
        }
    }
}
