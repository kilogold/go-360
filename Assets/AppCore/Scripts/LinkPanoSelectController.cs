﻿using UnityEngine;
using System.Collections;

namespace TourBuilder
{
	/// <summary>
	/// This class ensures that the UI controls displayed on screen
	/// are relevant to the state of the tour (init or standard).
	/// It is meant for recycling the pano select screen, since it
	/// serves two purposes.
	/// </summary>
		public class LinkPanoSelectController : MonoBehaviour
		{
			private ConstructorController constructorController;
			
			[SerializeField] private GameObject[] standardTourObject;
			[SerializeField] private GameObject[] initTourObject;

			void Awake()
			{
				constructorController = GameObject.FindObjectOfType<ConstructorController>();
			}

			void OnEnable ()
			{
				if( standardTourObject.Length != initTourObject.Length )
				{
					throw new UnityException("array mismatch");
				}

				bool isTourInitialized = constructorController.IsTourInitialized();

				for (int i = 0; i < initTourObject.Length; i++) 
				{
					standardTourObject[i].SetActive( isTourInitialized );
					initTourObject[i].SetActive( !isTourInitialized );
				}
			}
		}
}