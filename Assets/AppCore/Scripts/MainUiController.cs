﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace TourBuilder
{
	public class MainUiController : MonoBehaviour
	{
		/// <summary>
		/// The user interface screens.
		/// This might seem redundant, considering that the screens are 
		/// child transforms, yet it serves flexibility in sourcing the screens
		/// from some other hierarchy if needed.
		/// It is also readily usable for indexing.
		/// </summary>
		[SerializeField] private RectTransform[] uiScreens = null; 

		[SerializeField] private ConstructorController tourInstanceController;
		
		[SerializeField] private RectTransform saveSuccessScreen;
		[SerializeField] private RectTransform saveOverwriteConfirmationScreen;
		[SerializeField] private RectTransform saveFailedScreen;
		[SerializeField] private RectTransform saveScreen;
		[SerializeField] private InputField saveScreenInputField;

		void Awake()
		{
			int totalScreens = transform.childCount;

			uiScreens = new RectTransform[totalScreens];

			for (int i = 0; i < totalScreens; i++) 
			{
				uiScreens[i] = transform.GetChild(i).GetComponent<RectTransform>();	
			}
		}

		public void PresentUiScreen( RectTransform uiScreen )
		{
			if( uiScreens.Length == 0 )
			{
				Debug.LogError( "No screens are registerred." );
			}
			foreach (var item in uiScreens) 
			{
				item.gameObject.SetActive( item == uiScreen );
			}
		}

		public void AttemptSave( RectTransform saveScreen )
		{
			// TODO:
			// Have the UiController perform a "Save" method that ensures
			// no weird filenames are used (special characters, etc.).
			string filename = saveScreenInputField.text;
			bool doesFileExist = TourSaveLoadUtility.DoesTourBinaryExist( filename, tourInstanceController.TourSaveLoadCategory );

			// If we don't want to overwrite, and we find an existing file...
			if (saveScreen != saveOverwriteConfirmationScreen && doesFileExist ) 
			{
				Debug.Log("Existing save file found.");

				var title = saveOverwriteConfirmationScreen.Find("Title");

				title.GetComponent<Text>().text = 
					"'" + filename +"' already exists.\n" +
					"Overwrite?";

				PresentUiScreen(saveOverwriteConfirmationScreen);
			}
			else
			{
				try
				{
					tourInstanceController.SaveTour( filename );
					PresentUiScreen(saveSuccessScreen);
				}
				catch
				{
					PresentUiScreen(saveFailedScreen);
				}
			}
		}
	}
}
