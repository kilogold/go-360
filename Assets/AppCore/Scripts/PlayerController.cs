using UnityEngine;
using System.Collections;

[RequireComponent(typeof(TourNavigator))]
public class PlayerController : MonoBehaviour
{
    PlayerTour tourInstance;
    TourNavigator tourNavigator;

    // Use this for initialization
    void Start()
    {
        TourSaveLoadUtility utilSaveLoad = GameObject.FindObjectOfType<TourSaveLoadUtility>();
        tourInstance = new PlayerTour(utilSaveLoad);
        tourNavigator = GetComponent<TourNavigator>();
        tourNavigator.Initialize(tourInstance);
        tourNavigator.ResetRoomSphere();

		Debug.Log("Initialized a TourPlayer with " + tourInstance.GetRoomCount() + " rooms.");
    }
}
