using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GameObjectNote))]
public class GameObjectNoteEditor : Editor
{
    private bool isEditing;
    private bool previousDisplayOnScene;
    string textArea = string.Empty;
    Vector2 scrollPositionTextArea;

    public override void OnInspectorGUI()
    {
        GameObjectNote noteObj = (GameObjectNote)target;
        textArea = noteObj.note;

        isEditing = GUILayout.Toggle(isEditing, "EditMode");
        noteObj.displayOnScene = GUILayout.Toggle(noteObj.displayOnScene, "DisplayOnScene");
        if (previousDisplayOnScene != noteObj.displayOnScene)
        {
            SceneView.RepaintAll();
            previousDisplayOnScene = noteObj.displayOnScene;
        }


        scrollPositionTextArea = EditorGUILayout.BeginScrollView(scrollPositionTextArea, GUILayout.Height(150));
        {
            EditorStyles.textField.wordWrap = false;
            EditorStyles.textField.richText = true;
            GUIStyle textStyle = GUI.skin.GetStyle("HelpBox");

            GUILayout.FlexibleSpace();
            textArea = EditorGUILayout.TextArea(textArea, textStyle, GUILayout.ExpandHeight(true));

            if (isEditing)
            {
                noteObj.note = textArea;
            }
            else
            {
                Repaint();
            }

        }
        EditorGUILayout.EndScrollView();
    }

    // Create a 180 degrees wire arc with a ScaleValueHandle attached to the disc
    // that lets you modify the "shieldArea" var in the WireArcExample.js, also
    // lets you visualize some info of the transform

    void OnSceneGUI()
    {
        GameObjectNote noteObj = (GameObjectNote)target;

        if (noteObj.displayOnScene)
        {
            Handles.color = Color.blue;
            Handles.Label(
                noteObj.transform.position + Vector3.up * 2,
                noteObj.note,
                GUI.skin.GetStyle("HelpBox")
                );
        }
    }

}
