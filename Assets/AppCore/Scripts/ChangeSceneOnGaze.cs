using UnityEngine;
using System.Collections;

public class ChangeSceneOnGaze : MonoBehaviour
{
    [SerializeField]
    string sceneName;

    void OnGaze()
    {
        Application.LoadLevel(sceneName);
    }
}
