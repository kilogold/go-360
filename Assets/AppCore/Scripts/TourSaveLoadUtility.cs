using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using TourBuilder;

//TODO:
//Create a SelectedFileCache variable that acts like an indexer into the file.
//Maybe call it a TourDataIndexer. It just keeps offsets. This way, we don't have to iterate through
//everything. Instead we go straight for the offset within the Tour file.
public class TourSaveLoadUtility : MonoBehaviour
{
	/// <summary>
	/// The tour catalog.
	/// Key: tour category.
	/// Value: tour disk-filepath list
	/// </summary>
	private static Dictionary<string,List<string>> tourCatalog;

	public const float BinarySpecificationVersion = 0.2f;

	public static string SelectedFilepath { get; private set; }
	public static string SelectedCategory { get; private set; }
	
	private static string ApplicationPath 
	{ 
		get 
		{
			switch (Application.platform)
			{
			case RuntimePlatform.Android:
			case RuntimePlatform.IPhonePlayer:
				return Application.persistentDataPath;
			case RuntimePlatform.WindowsEditor:
			case RuntimePlatform.WindowsPlayer:
			case RuntimePlatform.OSXEditor:
			case RuntimePlatform.OSXPlayer:
				return Application.dataPath;
			default:
				throw new NotSupportedException("Unsupported Platform");
			}
		}
	}

	public static bool DoesTourBinaryExist( string filename, string category )
	{
		return File.Exists(GenerateTourFilepathFromFilename(filename, category));
	}

	public static string GenerateTourFilepathFromFilename( string filename, string category )
	{
		return ToursMainDirectory + "/" + category + "/" + filename;
	}

	public static string ToursMainDirectory
	{
		get { return ApplicationPath + "/Tours"; }
	}

	/// <summary>
	/// Initializes the tour catalog by navigating through all the tour directories.
	/// This should only execute once (by checking if the catalog is initialized).
	/// </summary>
    void Awake()
    {
		if( null == tourCatalog )
		{
			ScanDiskForTours();
		}
    }

	public void ScanDiskForTours()
	{
		tourCatalog = new Dictionary<string, List<string>>();

		if ( false == Directory.Exists(ToursMainDirectory)) 
		{
			Directory.CreateDirectory(ToursMainDirectory);
			return;
		}

		string[] tourDirectories = Directory.GetDirectories(ToursMainDirectory);
		
		foreach (string tourDirectory in tourDirectories) 
		{
			string directoryCategory = new DirectoryInfo(tourDirectory).Name;
			string[] availableTours = Directory.GetFiles(tourDirectory, "*.tour");
			tourCatalog.Add(directoryCategory, new List<string>(availableTours) );
		}

		// Rescan results in potential change/removal of current Filepath/Category.
		try 
		{	
			SetSelectedCategory(SelectedCategory);
		} 
		catch (UnityException) 
		{
			SelectedCategory = null;
		}

		try 
		{
			SetSelectedFilename(SelectedFilepath);
		} 
		catch (UnityException) 
		{
			SelectedFilepath = null;	
		}
	}

	public List<string> GetTourPathListForCategory( string category )
	{
		return tourCatalog[category];
	}

	public string GetTourFileName( int tourIndex, string category )
	{
		return Path.GetFileNameWithoutExtension(tourCatalog[category][tourIndex]);
	}

	/// <summary>
	/// Sets the selected filename. The filename must always exist.
	/// Otherwise, we have an error.
	/// </summary>
	/// <param name="availableFilenameIndex">Available filename index.</param>
    public static void SetSelectedFilename(int availableFilenameIndex)
    {
        if( availableFilenameIndex < 0 || availableFilenameIndex >= tourCatalog[SelectedCategory].Count)
        {
            throw new UnityException(
				"Filename index [" + availableFilenameIndex + "] does not exist in the " +
				"currently selected category (" + SelectedCategory + ")" );
        }

		SelectedFilepath = tourCatalog[SelectedCategory][availableFilenameIndex];

		Debug.Log("Currently selected save/load filename set to:\n" + SelectedFilepath);
    }

	public static void SetSelectedFilename(string fullFilePath)
	{	
		if( string.IsNullOrEmpty(fullFilePath) || false == tourCatalog[SelectedCategory].Contains( fullFilePath ) )
		{
			throw new UnityException("Selecting non-exsistent filename via filepath: \n" + fullFilePath);
		}
			
		SelectedFilepath = fullFilePath;
		Debug.Log("Currently selected save/load filename set to:\n" + SelectedFilepath);
	}

	public static void SetSelectedCategory(string category)
	{
		if( string.IsNullOrEmpty(category) || false == tourCatalog.ContainsKey( category ) )
		{
			throw new UnityException("\'" + category + "\' is not a valid category.");
		}

		SelectedCategory = category;

		Debug.Log("Currently selected save/load category set to:\n" + SelectedCategory);
	}

	public static void CreateCategory( string category )
	{
		if( false == tourCatalog.ContainsKey( category ) )
		{
			tourCatalog.Add(category, new List<string>() );
		}
	}

	public void WriteOut(byte[] tourBytes, string filePath )
	{
		string directory = Path.GetDirectoryName(filePath);
		if( false == Directory.Exists( directory ))
		{
			Directory.CreateDirectory( directory );
		}

		using (BinaryWriter writer = new BinaryWriter(File.Create(filePath)))
		{
			writer.Write(tourBytes);
			writer.Close();
		}
	}

    public void WriteOut(Tour tourInstance, string filePath)
    {
        RoomTextureLoader roomTextureLoader = RoomTextureLoaderFactory.CreateRoomTextureLoader();

		using (BinaryWriter writer = new BinaryWriter(File.Open(filePath, FileMode.Create)))
        {
			// Embed specification version.
			writer.Write( BinarySpecificationVersion.ToString("0.0") );

			//Consider using TimeOffset?
			//https://msdn.microsoft.com/en-us/library/bb384267.aspx
			writer.Write(DateTime.UtcNow.ToString());

            int tourRoomCount = tourInstance.GetRoomCount();
            writer.Write(tourRoomCount);

            for (int curNodeIndex = 0; curNodeIndex < tourRoomCount; curNodeIndex++)
            {
                TourNode roomNode = tourInstance.GetRoom(curNodeIndex);
                writer.Write( roomNode.RoomLinks.Count );
                
                foreach (TourNode.RawLinkData curLink in roomNode.RoomLinks)
                {
                    writer.Write(curLink.LinkRoomID);
                    writer.Write(curLink.LinkPosition.x);
                    writer.Write(curLink.LinkPosition.y);
                    writer.Write(curLink.LinkPosition.z);
                }

				byte[] textureBytes = roomTextureLoader.GetRoomTexture(roomNode.RoomTextureIndex).EncodeToJPG();
                writer.Write(textureBytes.Length);
                writer.Write(textureBytes);
            }

			Texture2D thumbnail = roomTextureLoader.GetRoomTexture(0).ResizeTexture( Texture2DExtensions.ImageFilterMode.Biliner, 0.25f );;
			byte[] thumbnailBytes = thumbnail.EncodeToJPG();
			writer.Write(thumbnailBytes.Length);
			writer.Write(thumbnailBytes);

            writer.Close();
        }

		Resources.UnloadUnusedAssets();
		GC.Collect();
    }
	
	public void ReadIn( ref ConstructorTour tourInstance )
	{
		Debug.Log("Loading from file.");
		
		try
		{
			using (BinaryReader reader = new BinaryReader(File.OpenRead(SelectedFilepath)))
			{
				float tourFileSpecVersion = float.Parse(reader.ReadString());
				if( tourFileSpecVersion < BinarySpecificationVersion )
				{
					throw new UnityException("Tour Binary File Spec Mismatch.\n" +
					                         "Version: " + tourFileSpecVersion + "\n" +
					                         "Expected: " + BinarySpecificationVersion );
				}

				// Read timestamp & advance the reader cursor. We don't need to validate 
				// anything for Tour Builder purposes. We just read the content.
				reader.ReadString();
			
				int tourRoomCount = reader.ReadInt32();
				
				tourInstance.ResetRooms();
				
				for (int curRoomIndex = 0; curRoomIndex < tourRoomCount; curRoomIndex++)
				{
					TourNode newTourNode = ParseSingleTourNodeFromBinary( reader );
					tourInstance.AddRoom( newTourNode);
					
					Debug.Log("Loading Rooms: " + (curRoomIndex / tourRoomCount) * 100 + "%");
				}
				
				reader.Close();
			}
		}
		catch(UnauthorizedAccessException e)
		{
			Debug.Log(e.Message);
			Debug.Log(e.Source);
			Debug.Log(e.InnerException);
		}
	}

    public TourNode ParseSingleTourNodeFromBinaryAtIndex( int index )
    {
        using (BinaryReader reader = new BinaryReader(File.OpenRead(SelectedFilepath)))
        {
            //Seek past the header. (Total room count Int32)
			SeekPastHeader(reader);

            for (int curRoomIndex = 0; curRoomIndex < index; curRoomIndex++)
            {
                SeekBinaryReaderToNextTourNode( reader );
            }

            return ParseSingleTourNodeFromBinary(reader);
        }
    }

	public TourNode ParseSingleTourNodeFromBinary( BinaryReader reader )
	{
		int roomTotalLinks = reader.ReadInt32();
		TourNode.RawLinkData[] linkRawData = new TourNode.RawLinkData[roomTotalLinks];
		
		for (int curLinkIndex = 0; curLinkIndex < roomTotalLinks; curLinkIndex++)
		{
			int linkRoomID = reader.ReadInt32();
			Vector3 linkPosition = new Vector3(
				reader.ReadSingle(),
				reader.ReadSingle(),
				reader.ReadSingle());
			
			// -1 for instance ID. It will be updated upon instantiation of link portal prefab.
			linkRawData[curLinkIndex] = new TourNode.RawLinkData(-1, linkRoomID, linkPosition);
		}
		long roomTextureByteLengthBinaryOffset = reader.BaseStream.Position;
		return new TourNode(linkRawData, roomTextureByteLengthBinaryOffset);
	}

	public byte[] ParseSingleTourNodeRoomTextureBytesFromBinaryAtIndex(int index)
	{
		using (BinaryReader reader = new BinaryReader(File.OpenRead(SelectedFilepath)))
		{
			SeekPastHeader( reader );
			
			for (int curRoomIndex = 0; curRoomIndex < index; curRoomIndex++)
			{
				SeekBinaryReaderToNextTourNode(reader);
			}
			
			int roomTotalLinks = reader.ReadInt32();
			const int linkRoomIdBytes = 4;
			const int linkPositionBytes = 4 * 3; // 4 bytes for x, y, and z
			const int linkDataBytes = linkRoomIdBytes + linkPositionBytes;
			
			reader.BaseStream.Seek(linkDataBytes * roomTotalLinks, SeekOrigin.Current);
			
			int textureBytesLength = reader.ReadInt32();
			
			return reader.ReadBytes(textureBytesLength);
		}
	}

    public byte[] ParseTourNodeTextureBytesFromBinaryOffset(long offset)
    {
        using (BinaryReader reader = new BinaryReader(File.OpenRead(SelectedFilepath)))
        {
            reader.BaseStream.Seek(offset, SeekOrigin.Begin);
            
            int textureBytesLength = reader.ReadInt32();

            return reader.ReadBytes(textureBytesLength);
        }
    }

	public byte[] ParseTourThumbnailTextureBytes()
	{
		using (BinaryReader reader = new BinaryReader(File.OpenRead(SelectedFilepath)))
		{
			int totalRooms = SeekPastHeader(reader);
			
			for (int curRoomIndex = 0; curRoomIndex < totalRooms; curRoomIndex++)
			{
				SeekBinaryReaderToNextTourNode( reader );
			}

			int thumbnailTextureBytes = reader.ReadInt32();

			return reader.ReadBytes(thumbnailTextureBytes);
		}
	}
		
	public int ParseTotalRoomsCount()
	{
		using (BinaryReader reader = new BinaryReader(File.OpenRead(SelectedFilepath)))
		{
			return SeekPastHeader(reader);
		}
	}

	public string ParseTourVersion()
	{
		using (BinaryReader reader = new BinaryReader(File.OpenRead(SelectedFilepath)))
		{
			return reader.ReadString();
		}
	}

	public string ParseTourTimestamp()
	{
		using (BinaryReader reader = new BinaryReader(File.OpenRead(SelectedFilepath)))
		{
			reader.ReadString(); // Binary Version
			return reader.ReadString(); // Timestamp;
		}
	}

    public void SeekBinaryReaderToNextTourNode( BinaryReader reader )
    {
        //If the reader is at the beginning of the file, we should seek past the header.
        if( reader.BaseStream.Position == 0 )
        {
			SeekPastHeader(reader);
        }

        int roomTotalLinks = reader.ReadInt32();
        const int linkRoomIdBytes = 4;
        const int linkPositionBytes = 4 * 3; // 4 bytes for x, y, and z
        const int linkDataBytes = linkRoomIdBytes + linkPositionBytes;

        reader.BaseStream.Seek(linkDataBytes * roomTotalLinks, SeekOrigin.Current);

        int textureBytesLength = reader.ReadInt32();
        reader.BaseStream.Seek(textureBytesLength, SeekOrigin.Current);
    }

	private Int32 SeekPastHeader( BinaryReader reader )
	{
		reader.BaseStream.Position = 0;
		reader.ReadString(); // skip version.
		reader.ReadString(); // skip timestamp.
		return reader.ReadInt32(); // Return the room count.
	}
}
