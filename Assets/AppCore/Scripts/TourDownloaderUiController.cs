using UnityEngine;
using ProgressBar;
using System.Collections;

public class TourDownloaderUiController : MonoBehaviour 
{
	public ProgressRadialBehaviour singleTourProgress;
	public ProgressBarBehaviour tourSuiteProgress;
	public TourDownloader tourDownloader;
	public UiPanelFader fader;

	private int totalToursToLoad;

	/// <summary>
	/// The category to load when the UI panel becomes visible.
	/// </summary>
	private string categoryToLoadUponVisible;

	void Awake()
	{
		fader.FadeComplete += OnFadeComplete;
		tourDownloader.TourLoadedEvent += OnTourLoaded;
		tourDownloader.TourDownloadTickEvent += OnTourDownloadTick;
		tourDownloader.TourLoadingBeginEvent += OnLoadingBegins;
		tourDownloader.TourLoadingEndEvent += OnLoadingEnds;
	}

	void OnDestroy()
	{
		fader.FadeComplete -= OnFadeComplete;
		tourDownloader.TourLoadedEvent -= OnTourLoaded;
		tourDownloader.TourDownloadTickEvent -= OnTourDownloadTick;
		tourDownloader.TourLoadingBeginEvent -= OnLoadingBegins;
		tourDownloader.TourLoadingEndEvent -= OnLoadingEnds;
	}
	
	public void LoadFromCategoryWhenReady( string category )
	{
		categoryToLoadUponVisible = category;
	}

	private void OnFadeComplete( CanvasGroup visiblePanel )
	{
		if( GetComponent<CanvasGroup>() == visiblePanel )
		{
			tourDownloader.LoadToursFromCategory(categoryToLoadUponVisible);
			categoryToLoadUponVisible = null;
		}
		else
		{
			tourSuiteProgress.ForceReset();
		}
	}

	private void OnTourDownloadTick(float progress)
	{
		singleTourProgress.Value = progress;
	}

	private void OnIndexDownloadTickEvent(float percentage)
	{
		// Same thing... Should probably refactor...
		OnIndexDownloadTickEvent(percentage);
	}

	private void OnLoadingEnds()
	{
		// This will usually happen when we've indexed a category that has no new updates.
		// There will be no progress events about loading the tours because there's no new tours 
		// to load. Instead, we set the progress to 100, and let the progress bar events take over.
		if( tourSuiteProgress.Value < 100.0f )
		{
			tourSuiteProgress.Value = 100.0f;
		}
	}

	private void OnLoadingBegins(int totalTours)
	{
		totalToursToLoad = totalTours;
	}

	private void OnTourLoaded(int loadedTourIterCount)
	{
		//We add a 1 because the iter count is zero-based.
		float percentageScalar = ( (float)loadedTourIterCount + 1.0f ) / totalToursToLoad;
		float percentage =  percentageScalar * 100.0f;
		tourSuiteProgress.Value = percentage;
	}
}
