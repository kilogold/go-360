using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/********************
 * This class is intended to work as the actual map data for the tour.
 * Each 'TourNode' represents a room.
 * Although the hierarchy will be populated with scene elements that 
 * constitute the tour, those are built based on the TourNode data.
 ********************/
public class TourNode
{
    public class RawLinkData
    {
        /// <summary>
        /// The room the link is pointing to.
        /// </summary>
        public int LinkRoomID {get; set;}
        public Vector3 LinkPosition { get; set; }

        /// <summary>
        /// ID to the scene hiararchy link GameObject.
        /// Used for identification purposes when deleting, editing links.
        /// </summary>
        public int LinkDataInstanceID { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="TourNode+RawLinkData"/> class.
		/// </summary>
		/// <param name="linkDataInstanceIDIn">ID to the scene hiararchy link GameObject.</param>
		/// <param name="linkRoomIDIn">ID to the room this links to</param>
		/// <param name="linkPositionIn">Link GameObject position within the room sphere.</param>
        public RawLinkData(int linkDataInstanceIDIn, int linkRoomIDIn, Vector3 linkPositionIn)
        {
            LinkRoomID = linkRoomIDIn;
            LinkPosition = linkPositionIn;
            LinkDataInstanceID = linkDataInstanceIDIn;
        }
    }

    /// <summary>
    /// Offset for the save file.
    /// The TOUR save file is in binary, and we cache the byte offset to read 
    /// from later without having to parse the entire binary file every time.
    /// This is only filled in when loading from binary file (usually only the TourPlayer)
    /// </summary>
    public long RoomTextureByteLengthBinaryOffset {get; private set;}

	/// <summary>
	/// Index of the room texture.
	/// The Tour builder needs to keep track of what texture the room will have for 
	/// serialization purposes. Instead of storing the whole image in memory and holding a
	/// reference to it, we hold the index for the RoomTextureLoader.
	/// This is only filled in when running the TourBuilder.
	/// </summary>
	public int RoomTextureIndex {get; private set;}
    
    /// <summary>
    /// List of links to other rooms from this room.
    /// Must be a List<> because the tour constructor can add/remove
    /// </summary>
    public List<RawLinkData> RoomLinks { get; private set; }

    public TourNode( RawLinkData[] linkDataList, long roomTextureByteLengthBinaryOffset = -1)
    {
        RoomLinks = new List<RawLinkData>(linkDataList);
        RoomTextureByteLengthBinaryOffset = roomTextureByteLengthBinaryOffset;
    }


    public TourNode( int roomTextureIndex = -1 )
    {
        RoomLinks = new List<RawLinkData>();
        RoomTextureByteLengthBinaryOffset = -1;
		RoomTextureIndex = roomTextureIndex;
    }

    public void SetLinkDataList( LinkData[] linkDataListIn )
    {
        // Fill out the array with the incoming Link Data
        RoomLinks = new List<RawLinkData>(linkDataListIn.Length);

        for (int curLinkIndex = 0; curLinkIndex < linkDataListIn.Length; curLinkIndex++)
        {
            RoomLinks[curLinkIndex] = new RawLinkData(
                linkDataListIn[curLinkIndex].GetInstanceID(),
                linkDataListIn[curLinkIndex].LinkedRoomID,
                linkDataListIn[curLinkIndex].transform.position);
        }
    }

    public RawLinkData GetRoomLinkForInstanceID( int instanceID )
    {
       return RoomLinks.Find(e => e.LinkDataInstanceID == instanceID);
    }
}
