﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace TourBuilder
{
	public class UiController_Actions : MonoBehaviour
	{
		[SerializeField]
		private ConstructorController tourBuilderController;

		[SerializeField]
		private Button NavigateBtn;


		public void OnNavigateToLink()
		{
			tourBuilderController.NavigateToSelectedRoom();
		}

		void FixedUpdate()
		{
			//NavigateBtn.interactable = (tourBuilderController.SelectedLink != null);
		}
	}
}
