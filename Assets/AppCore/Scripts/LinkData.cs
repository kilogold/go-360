using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class LinkData : GazeInteractionReceiver
{
    public int LinkedRoomID;
    private TourNavigator tourNavigator;
	private TapGesture tapGesture;

    void Awake()
    {
        // Set default init value
        LinkedRoomID = -1;
		tapGesture = GetComponent<TapGesture>();
		tapGesture.Tapped += OnTap;    
	}

	void OnDestroy()
	{
		tapGesture.Tapped -= OnTap;
	}

    void Start()
    {
        tourNavigator = GameObject.FindObjectOfType<TourNavigator>();
    }

    void OnGaze()
    {
		//Should only be running during Player Mode, NOT Builder Mode.
        tourNavigator.NavigateToRoom(LinkedRoomID);
    }

	void OnTap( object sender, System.EventArgs e )
	{
		OnGaze();
	}
}
