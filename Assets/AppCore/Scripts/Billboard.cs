using UnityEngine;
using System.Collections;

public class Billboard : MonoBehaviour
{
	[SerializeField]
    Transform target;

    void LateUpdate()
    {
        transform.rotation = target.rotation;
    }
}
