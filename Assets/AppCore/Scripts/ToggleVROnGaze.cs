using UnityEngine;
using System.Collections;

public class ToggleVROnGaze : MonoBehaviour
{
    private Cardboard cardboard;

    void Start()
    {
        cardboard = GameObject.FindObjectOfType<Cardboard>();
    }

    void OnGaze()
    {
		if( cardboard )
        	cardboard.VRModeEnabled = !cardboard.VRModeEnabled;
    }
}
