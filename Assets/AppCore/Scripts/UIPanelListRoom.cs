using UnityEngine;
using UnityEngine.UI;
using TourBuilder;

public class UIPanelListRoom : MonoBehaviour
{    
	[SerializeField] private ConstructorController constructorController;
	[SerializeField] private RectTransform confirmationScreen;
	[SerializeField] private MainUiController uiController;
	[SerializeField] private GameObject buttonPrefab;
	private RectTransform scrollContentTransform;
	private RoomTextureLoader textureLoader;

	void Awake()
	{
		scrollContentTransform = GetComponent<ScrollRect>().content;
		textureLoader = RoomTextureLoaderFactory.CreateRoomTextureLoader();
	}

	/// <summary>
	/// Every time we enable, we populate the list with the currently 
	/// loaded data, as this is where we will pick our rooms from.
	/// </summary>
    void OnEnable()
    {
        // Clean out before use
        foreach (RectTransform childTransform in scrollContentTransform)
        {
            Destroy(childTransform.gameObject);
        }

		// Get the list of room texture indices to populate the scroll container with.
		// TODO: Modify 'GetRoomTextureIndiciesFromTour' to receive immutable TourNode[], 
		// where TourNode keeps some sort of name on the room. Some images may look very 
		// similar, and a name can help tell them apart, even if it's the filename itself.
		// Some inspiration:
		// https://social.msdn.microsoft.com/Forums/vstudio/en-US/02ee84e5-bca1-4cb5-a71e-dbca9b21eaf8/dynamically-making-object-immutable
		var roomTextureIndices = constructorController.GetRoomTextureIndiciesFromTour();

		for( int i = 0; i < roomTextureIndices.Length; i++)
        {
			Texture2D roomTexture = textureLoader.GetRoomTexture(roomTextureIndices[i]);
            GameObject newButton = Instantiate(buttonPrefab);

                newButton.GetComponent<Image>().sprite =
                Sprite.Create(
                    roomTexture,
                    new Rect(0, 0, roomTexture.width, roomTexture.height),
                    new Vector2(0.5f, 0.5f));

            // HACK: 
            // For some reason, lambda expression somehow caches the value of the parameter
            // when we use the iteration variable, so to fix it, we use a new variable with
            // the copied value and pass that as argument instead.
            // Hit source: http://answers.unity3d.com/questions/908847/passing-a-temporary-variable-to-add-listener.html
            int lambdaHackIndex = i;
            newButton.GetComponent<Button>().onClick.AddListener(() => EntryClicked(lambdaHackIndex));

			newButton.transform.SetParent(scrollContentTransform);

			// For some reason, when we set the parent, the scale changes too.
			// We need to ensure this doesn't happen if we want to retain the designed values.
			newButton.transform.localScale = Vector3.one;
		}
    }

    public void EntryClicked(int selectedEntryIndex)
    {
        Debug.Log("Existing Room Selected Index: " + selectedEntryIndex);

		//Set the image on the confirmation screen.
		Image buttonImage = scrollContentTransform.GetChild(selectedEntryIndex).GetComponent<Image>();
		Image previewImage = confirmationScreen.Find("RoomPreview").GetComponent<Image>();
		previewImage.sprite = buttonImage.sprite;
		uiController.PresentUiScreen(confirmationScreen);

		previewImage.GetComponent<LinkRoomSelectionMeta>().TourRoomIndex = selectedEntryIndex;
    }
}
