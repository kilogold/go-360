using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
public class ScrollRectInput : MonoBehaviour
{
    ScrollRect scrollRect;

    [SerializeField]
    float sensitivity;

    void Start()
    {
        scrollRect = GetComponent<ScrollRect>();
    }


    Vector2 lastFrameTouchPosition = Vector2.zero;

    void Update()
    {
        // Only perform this action on mobile (Specifically GearVR).
        // We want to be able to run mouse-connected platform without driving it crazy.
        // Scrolling in the GearVR is not exactly how we'd expect it to behave because once the finger
        // releases the touch pad, it assumes that is the position it is currently at. Next time we touch the 
        // pad it will have an inacurate delta for the first frame.
        if( Application.isMobilePlatform )
        {
            if( Input.GetMouseButtonDown(0))
            {
                lastFrameTouchPosition = Input.mousePosition;
            }

            Vector2 currentTouchPosition = Input.mousePosition;

            float vertDelta = currentTouchPosition.y - lastFrameTouchPosition.y;
            float timescaledVertDelta = vertDelta * Time.deltaTime;

            scrollRect.verticalNormalizedPosition = Mathf.Clamp01(scrollRect.verticalNormalizedPosition + (timescaledVertDelta * sensitivity));

            lastFrameTouchPosition = currentTouchPosition;
        }
    }
}
