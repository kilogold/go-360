﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

namespace TouchScript.Examples.CameraControl 
{
	public class CameraController : MonoBehaviour 
	{
		public ScreenTransformGesture ManipulationGesture;
		public float RotationSpeed = 200f;
		
		[SerializeField]
		private Transform cam;
		
		private void OnEnable() 
		{
			ManipulationGesture.Transformed += manipulationTransformedHandler;
		}
		
		private void OnDisable() 
		{
			ManipulationGesture.Transformed -= manipulationTransformedHandler;
		}
		
		private void manipulationTransformedHandler(object sender, System.EventArgs e)
		{
			var rotation = Quaternion.Euler(ManipulationGesture.DeltaPosition.y / Screen.height * RotationSpeed, 
			                                -ManipulationGesture.DeltaPosition.x / Screen.width * RotationSpeed, 
			                                ManipulationGesture.DeltaRotation);
			cam.localRotation *= rotation;
			cam.rotation = Quaternion.Euler(
				cam.rotation.eulerAngles.x,
				cam.rotation.eulerAngles.y,
				0 );
		}
	}
}