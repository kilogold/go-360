using UnityEngine;
using UnityEngine.UI;
using TourBuilder;
using System;

public class UIPanelListPhotosphere : MonoBehaviour
{    
	[SerializeField] private RectTransform confirmationScreen;
	[SerializeField] private MainUiController uiController;

    public RoomTextureLoader textureLoader;
    public GameObject buttonPrefab;
    public RectTransform scrollRectTransform;
    public bool compressThumbnailsOnLoad;

    // Use this for initialization
    void Start()
    {
        // Clean out before use
        foreach (RectTransform childTransform in scrollRectTransform)
        {
            Destroy(childTransform.gameObject);
        }

        textureLoader = RoomTextureLoaderFactory.CreateRoomTextureLoader();

        int totalRoomTextures = textureLoader.RoomTexturesCount();

        for (int i = 0; i < totalRoomTextures; i++)
        {
            Texture2D roomTexture = textureLoader.GetRoomTexture(i);
            GameObject newButton = Instantiate(buttonPrefab);

            if (compressThumbnailsOnLoad)
            {
				try
				{
	                Texture2D smallRoomTexture = roomTexture.ResizeTexture(Texture2DExtensions.ImageFilterMode.Average, 0.1f);

	                Destroy(roomTexture);

	                newButton.GetComponent<Image>().sprite =
	                    Sprite.Create(
	                        smallRoomTexture,
	                        new Rect(0, 0, smallRoomTexture.width, smallRoomTexture.height),
	                        new Vector2(0.5f, 0.5f));

	                Resources.UnloadUnusedAssets();
				}
				catch(OutOfMemoryException e)
				{
					Debug.LogWarning(
						"Ran out of memory. Only load what we have so far.\n" +
						"Exception:\n" +
						e.Message );

					Destroy (newButton); // Cancel the incomplete button.

					return;
				}
        	}
            else
            {
                newButton.GetComponent<Image>().sprite =
                Sprite.Create(
                    roomTexture,
                    new Rect(0, 0, roomTexture.width, roomTexture.height),
                    new Vector2(0.5f, 0.5f));
            }

            // HACK: 
            // For some reason, lambda expression somehow caches the value of the parameter
            // when we use the iteration variable, so to fix it, we use a new variable with
            // the copied value and pass that as argument instead.
            // Hit source: http://answers.unity3d.com/questions/908847/passing-a-temporary-variable-to-add-listener.html
            int lambdaHackIndex = i;
            newButton.GetComponent<Button>().onClick.AddListener(() => EntryClicked(lambdaHackIndex));

			newButton.transform.SetParent(scrollRectTransform);

			// For some reason, when we set the parent, the scale changes too.
			// We need to ensure this doesn't happen if we want to retain the designed values.
			newButton.transform.localScale = Vector3.one;
		}
    }

    public void EntryClicked(int selectedEntryIndex)
    {
        Debug.Log("Room Texture Selected Index: " + selectedEntryIndex);

		//Set the image on the confirmation screen.
		Image buttonImage = scrollRectTransform.GetChild(selectedEntryIndex).GetComponent<Image>();
		Image previewImage = confirmationScreen.Find("PanoPreview").GetComponent<Image>();
		previewImage.sprite = buttonImage.sprite;
		uiController.PresentUiScreen(confirmationScreen);

		previewImage.GetComponent<LinkPanoSelectionMeta>().RoomTextureLoaderImageIndex = selectedEntryIndex;
    }
}
