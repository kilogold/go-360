using UnityEngine;
using System.Collections;

public enum InteractionMode
{
    Input, // Awaits for input to interact 
    Timer  // Awaits a countdown to interact
}

public class GazeInteractionReceiver : MonoBehaviour
{
    public InteractionMode interactionMode = InteractionMode.Timer;
    public float interactionTimeThreshold;

    public void ExecuteOnGaze()
    {
        SendMessage("OnGaze");
    }
}
