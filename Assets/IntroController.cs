﻿using UnityEngine;
using System.Collections;

public class IntroController : MonoBehaviour
{
	void Start()
	{
		PlayerPrefs.SetInt(CameraSelector.PlayerPrefs_Mode,(int)CAMERA_CONFIG.GearVR);
		TourSaveLoadUtility.SetSelectedCategory("Parallel18");
		TourSaveLoadUtility.SetSelectedFilename(0);
	}

	public void TransitionToTour()
	{
		Application.LoadLevel("TourPlayer");
	}

	void Update()
	{
		if( Input.GetButtonDown("Fire1") )
		{
			GetComponent<Animator>().SetTrigger("StartSequence");
		}
	}
}
